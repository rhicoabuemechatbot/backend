<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\Employee;
use App\Models\User;
use App\Models\Designation;
use App\Models\Clusters;
use App\Models\Time_Track;

use App\Traits\RegisterUser;
use App\Traits\FindDesignationById;
use PDF;

class EmployeeController extends Controller
{
    use RegisterUser;
    use FindDesignationById;

    public function storeEmployee(Request $request){
        // validation goes here
        $employee = Employee::create($request->except([
            'email',
            'password',
            'password_confirmation',
            'name'
        ]));

        $user = $request->only([
            'email',
            'username',
            'password',
            'password_confirmation',
            'name'
        ]);

        $employee_id = $employee->id;
        $user_id = $this->register(collect($user), $employee_id);

        if($user_id && $employee_id){
            $this->updateUserWithEmployeeId($user_id, $employee_id);
        }

        return response()->json($employee, 200);
    }

    public function getallEmployees(){
        $employees = Employee::all();
        return $employees;
    }

    public function getSelectedEmployees(Request $request){
        $name = $request->name;
        $employee = Employee::where('lastName', $name)->get();

        return $employee;
    }

    public function updateEmployee(Request $request)
    {
        $employee_id = $request->id;
        $employee = Employee::where('id',$employee_id)->update($request->except([
            'email',
            'username',
            'password',
            'password_confirmation',
            'name',
            'designation_name',
            'cluster_name'
        ]));

        return $employee;
    }

    public function deleteEmployee(Request $request){
        $employee_id = $request->id;
        Employee::destroy($employee_id);

        return response()->json([
            'message'=>'User successfully deleted!'
        ]);
    }

    private function updateUserWithEmployeeId($id, $employee_id){
        $user = User::findOrFail($id);
        $user->employee_id = $employee_id;
        $user->save();

        return response()->json([
            'message'=>'User successfully updated!',
            'data'=>$user
        ]);
    }

    public function downloadPDF(Request $request) {
        $employee_id = $request->id;
        $name = $request->name;
        $day = $request->dayPDF;
        $month = $request->monthPDF;
        $monthString = $request->monthStringPDF;

        $data = Time_Track::where('employee_id', $employee_id)
                ->whereDay('created_at', '<=', $day)
                ->whereMonth('created_at', '=', $month)
                ->get();

        $span_day = "1-15";

        if($day == '15'){
            $span_day = "1-15";
        }
        else{
            $span_day = "1-31";
        }

        $array = collect([]);
        $employee_data = collect([]);

        foreach($data as $data){
            $morning = strtotime($data->morning_time_in);
            $afternoon = strtotime($data->afternoon_time_in);

            if($morning != NULL){
                $datetime_day = getDate($morning);
                $array->push($datetime_day['mday']);
            } else if ($afternoon != NULL){
                $datetime_day = getDate($afternoon);
                if(!$array->contains($datetime_day['mday'])){
                    $array->push($datetime_day['mday']);
                }
            }
        }

        ///iterate to all days
        for($i=1; $i<=$day; $i++){
            if($array->contains($i)){
                ///iterate to all days present
                for($j=0; $j<=count($array)-1; $j++){
                    if($array[$j] == $i){
                        $all_data = $data->where('employee_id', $employee_id)
                                    ->get();
                        $morning = strtotime($all_data[$j]->morning_time_in);
                        $afternoon = strtotime($all_data[$j]->afternoon_time_in);

                        if($morning != NULL){
                            $employee_data->push($all_data[$j]);
                        } else if ($afternoon != NULL){
                            $employee_data->push($all_data[$j]);
                        }
                    }
                }
            }
            else{
                $employee_data->push('');
            }
        }

        view()->share('employee',$employee_data);
        view()->share('details',$name);
        view()->share('span_day', $span_day);
        view()->share('month', $monthString);

        $pdf = PDF::loadView('pdf', $data);

        //array(0,0,width,height)
        $customPaper = array(0,0,900,1100);
        $pdf->setPaper($customPaper);

        // return $pdf->download('pdf_file.pdf');
        $output = $pdf->output();

        return new Response($output, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' =>  'inline; filename="time.pdf"',
        ]);
    }

    public function downloadBulkPDF(Request $request) {
        return $request;
    }
}
