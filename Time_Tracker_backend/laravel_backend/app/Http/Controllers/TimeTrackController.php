<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Time_Track;

class TimeTrackController extends Controller
{
    public function timeIn(Request $request)
    {
        $time_in = Time_Track::create($request->all());

        return $time_in;
    }

    public function updateTime(Request $request)
    {
        $timetrack_id = $request->id;
        $update_time = $request->updated_time;

        $timetrack = Time_Track::findOrFail($timetrack_id);

        $morning_time_in = $timetrack->morning_time_in;
        $morning_time_out = $timetrack->morning_time_out;
        $afternoon_time_in = $timetrack->afternoon_time_in;
        $afternoon_time_out = $timetrack->afternoon_time_out;

        //can we use switch statement for this?
        if($morning_time_in != NULL){
            if($morning_time_out == NULL){
                $timetrack->morning_time_out = $update_time;
                $timetrack->save();

                return $timetrack;
            }
            else if($afternoon_time_in == NULL){
                $timetrack->afternoon_time_in = $update_time;
                $timetrack->save();

                return $timetrack;
            }
            else if($afternoon_time_out == NULL){
                $timetrack->afternoon_time_out = $update_time;
                $timetrack->save();

                return $timetrack;
            }
        }
        else{
            if($afternoon_time_in == NULL){
                $timetrack->afternoon_time_in = $update_time;
                $timetrack->save();

                return $timetrack;
            }
            else if($afternoon_time_out == NULL){
                $timetrack->afternoon_time_out = $update_time;
                $timetrack->save();

                return $timetrack;
            }
        }

        //handle null value
        // return <something>
    }

    public function overtime(Request $request)
    {
        $timetrack_id = $request->id;
        $update_time = $request->updated_time;

        $timetrack = Time_Track::findOrFail($timetrack_id);

        $overtime_time_in = $timetrack->overtime_time_in;
        $overtime_time_out = $timetrack->overtime_time_out;

        //can we use switch statement for this?
        if($overtime_time_in == NULL){
            $timetrack->overtime_time_in = $update_time;
            $timetrack->save();

            return $timetrack;
        }
        else if($overtime_time_out == NULL){
            $timetrack->overtime_time_out = $update_time;
            $timetrack->save();

            return $timetrack;
        }

        //handle null value
        // return <something>
    }

    public function checkIfEmpty(Request $request){
        $timetrack_id = $request->id;

        $timetrack = Time_Track::findOrFail($timetrack_id);

        $checker_morning = $timetrack->morning_time_in;
        $checker_afternoon = $timetrack->afternoon_time_in;
        $checker_afternoon_out = $timetrack->afternoon_time_out;

        return response()->json([
            'morning'=>$checker_morning,
            'afternoon'=>$checker_afternoon,
            'afternoon_time_out'=>$checker_afternoon_out
        ]);
    }
}
