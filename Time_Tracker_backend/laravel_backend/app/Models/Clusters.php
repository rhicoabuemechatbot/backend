<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Clusters extends Model
{
    use Uuids;

    protected $table = 'clusters';

    protected $fillable = [
        'clusters',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
