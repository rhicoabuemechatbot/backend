<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Uuids;
use App\Traits\FindDesignationById;
use App\Traits\FindClusterById;

class Employee extends Model
{
    use Uuids;
    use FindDesignationById;
    use FindClusterById;

    protected $fillable = [
        'firstName',
        'middleName',
        'lastName',
        'employee_id',
        'suffix',
        'designation_id',
        'clusters_id',
        'employment_status',
        'cellphone_number',
        'email',
        'address',
        'birthdate',
        'activation_status',
        'work_mode'
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $appends = [
        'designation_name',
        'cluster_name'
    ];

    public function designation_id(){
        return $this->belongsTo('App\Models\Employee', 'designation_id');
    }

    public function getDesignationNameAttribute()
    {
        $designation_id = $this->designation_id;
        $designation = $this->findDesignationById($designation_id);

        return $designation;
    }

    public function cluster_id(){
        return $this->belongsTo('App\Models\Employee', 'clusters_id');
    }

    public function getClusterNameAttribute()
    {
        $cluster_id = $this->clusters_id;
        $cluster = $this->findClusterById($cluster_id);

        return $cluster;
    }
}
