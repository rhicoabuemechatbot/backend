<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->string('employee_id');
            $table->string('firstName');
            $table->string('middleName')->nullable();
            $table->string('lastName');
            $table->string('suffix')->nullable();
            $table->string('cellphone_number')->nullable();
            $table->date('birthdate');
            $table->string('address');
            $table->enum('employment_status', ['Job Order', 'Regular', 'Contractual']);
            $table->enum('activation_status', ['Active', 'Inactive']);
            $table->boolean('isApproved')->default(0);
            $table->enum('work_mode', ['On Premise', 'Work from Home', 'Official Business'])->default('On Premise');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
