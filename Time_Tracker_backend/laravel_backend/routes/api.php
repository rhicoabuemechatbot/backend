<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TimeTrackController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization, Accept,charset,boundary,Content-Length');
// header('Access-Control-Allow-Origin: *');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'employee'
], function ($router) {
    Route::post('/createEmployees', 'App\Http\Controllers\EmployeeController@storeEmployee');
    Route::get('/getEmployees', 'App\Http\Controllers\EmployeeController@getallEmployees');
    Route::post('/getSelectedEmployees', 'App\Http\Controllers\EmployeeController@getSelectedEmployees');
    Route::put('/updateEmployee', 'App\Http\Controllers\EmployeeController@updateEmployee');
    Route::post('/deleteEmployee', 'App\Http\Controllers\EmployeeController@deleteEmployee');

    Route::post('/createDesignation', 'App\Http\Controllers\DesignationController@storeDesignation');
    Route::get('/getDesignations', 'App\Http\Controllers\DesignationController@getAllDesignation');
    Route::put('/updateDesignation', 'App\Http\Controllers\DesignationController@updateDesignation');
    Route::post('/deleteDesignation', 'App\Http\Controllers\DesignationController@deleteDesignation');

    Route::post('/createClusters', 'App\Http\Controllers\ClustersController@storeClusters');
    Route::get('/getClusters', 'App\Http\Controllers\ClustersController@getAllClusters');
    Route::put('/updateCluster', 'App\Http\Controllers\ClustersController@updateCluster');
    Route::post('/deleteCluster', 'App\Http\Controllers\ClustersController@deleteCluster');

    Route::post('/downloadPDF', 'App\Http\Controllers\EmployeeController@downloadPDF');
    Route::post('/downloadBulkPDF', 'App\Http\Controllers\EmployeeController@downloadBulkPDF');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'user'

], function($router){
    Route::post('/findEmailByUsername', [UserController::class, 'findEmailByUsername']);
    Route::put('/updateUser/{id}', [UserController::class, 'updateUserWithEmployeeId']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'time'

], function($router){
    Route::post('/timein', [TimeTrackController::class, 'timeIn']);
    Route::put('/updateTime', [TimeTrackController::class, 'updateTime']);
    Route::post('/checkifempty', [TimeTrackController::class, 'checkIfEmpty']);
    Route::put('/overtime', [TimeTrackController::class, 'overtime']);
});
